if (Meteor.isClient){
  var options = {
  keepHistory: 1000 * 60 * 5,
  localSearch: true
};
var fields = ['title', 'description'];

WebsiteSearch = new SearchSource('websites', fields, options);

Template.searchResult.helpers({
  getWebsites: function() {
    return WebsiteSearch.getData({
      transform: function(matchText, regExp) {
        return matchText.replace(regExp, "<b>$&</b>")
      },
      sort: {isoScore: -1}
    });
  },

  isLoading: function() {
    return WebsiteSearch.getStatus().loading;
  }
});
Template.searchBox.events({
  "keyup #search-box": _.throttle(function(e) {
    var text = $(e.target).val().trim();
    WebsiteSearch.search(text);
  }, 200)
});
}

if (Meteor.isServer){
  SearchSource.defineSource('websites', function(searchText, options) {
  var options = {sort: {isoScore: -1}, limit: 20};

  if(searchText) {
    var regExp = buildRegExp(searchText);
    var selector = {$or: [
      {title: regExp},
      {description: regExp}
    ]};

    return Websites.find(selector, options).fetch();
  } else {
    return Websites.find({}, options).fetch();
  }
});

function buildRegExp(searchText) {
  // this is a dumb implementation
  var parts = searchText.trim().split(/[ \-\:]+/);
  return new RegExp("(" + parts.join('|') + ")", "ig");
}
}
