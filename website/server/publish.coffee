Meteor.publish 'websites',->
  Websites.find()

Meteor.publish 'website_single', (id) ->
  check id, String
  Websites.find _id: id
