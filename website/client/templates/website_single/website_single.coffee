Template.website_single.onCreated ->
  self = this
  self.autorun ->
    id = FlowRouter.getParam('id')
    self.subscribe 'website_single', id

Template.website_single.helpers(
  website: ->
    id = FlowRouter.getParam('id')
    Websites.findOne({_id:id})
  getUser: (user_id) ->
    user = Meteor.users.findOne(_id: user_id)
    if user
      user.username
    else
      'anon'
)

Template.website_single.events
  'click #addComment' : (event) ->
    $('#addCommentModal').modal('show')
