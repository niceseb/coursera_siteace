Template.website_item.events
  'click .js-upvote': (event) ->
    website_id = @_id
    Websites.update { '_id': website_id }, $inc: 'yes': +1
    console.log 'Up voting website with id ' + website_id
    false
    # prevent the button from reloading the page
  'click .js-downvote': (event) ->
    # example of how you can access the id for the website in the database
    # (this is the data context for the template)
    website_id = @_id
    console.log 'Down voting website with id ' + website_id
    Websites.update { '_id': website_id }, $inc: 'no': -1
    false
    # prevent the button from reloading the page
  'click #removeWebsite' :(event) ->
    website_id = @_id
    Websites.remove {'_id':website_id}

  'click #js-sessions' :(event)->
    website_id = @._id
    Session.set 'selectedPage', website_id
    selectedPage = Session.get 'selectedPage'
