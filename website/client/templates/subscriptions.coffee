Template.website_item.onCreated ->
  self = this
  self.autorun ->
    self.subscribe 'websites'

Template.website_list.onCreated ->
  self = this
  self.autorun ->
    self.subscribe 'websites'


Template.website_list.helpers(
  websites : ->
    Websites.find {}, sort:
      yes: -1
)
