Template.website_form.events
  'click .js-toggle-website-form': (event) ->
    $('#website_form').toggle 'slow'
    return
  'submit .js-save-website-form': (event) ->
    # here is an example of how to get the url out of the form:
    url = event.target.url.value
    title = event.target.title.value
    description = event.target.description.value
    Websites.insert
      title: title
      url: url
      description: description
      createdOn: new Date
      votes:[]
      comments:[]
    #  put your website saving code in here!
    false
