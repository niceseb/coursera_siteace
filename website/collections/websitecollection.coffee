@Websites = new Mongo.Collection 'websites'

Websites.allow
  insert: (userId, doc) ->
    ! !userId
  update: (userId, doc) ->
    ! !userId
  remove: (userId,doc)->
    ! !userId

Comment = new SimpleSchema(
  title:
    type:String
    label:"title"
  author:
    type:String
    label:"Author"
    autoform:
      type:"hidden"
    autoValue:->
      @.userId
  createdat:
    type:Date
    label:"Created At"
    autoform:
      type:"hidden"
    autoValue:->
      new Date())



WebsiteSchema = new SimpleSchema(
  title:
    type:String
    label:"Title"
  url:
    type:String
    label:'Url'
  votes:
    type:Number
    label:'votes'
    optional: true
    autoform:
      type:'hidden'

  yes:
    type:Number
    label:'yes'
    optional: true
    autoform:
      type:'hidden'

  no:
    type:Number
    label:'no'
    optional: false
    autoform:
      type:'hidden'

  description:
    type:String
    label:"Description"

  comment:
    type:[Comment]
    optional:true
  author:
    type:String
    label:"Author"
    optional:true
    autoform:
      type:"hidden"
    autoValue:->
      @.userId
  createdat:
    type:Date
    label:"Created At"
    autoform:
      type:"hidden"
    autoValue:->
      new Date())



Websites.attachSchema(WebsiteSchema)
