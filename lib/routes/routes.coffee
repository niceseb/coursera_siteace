FlowRouter.route '/',
  name: 'home'
  action: ->
    BlazeLayout.render 'HomeLayout', main: 'website_list'

FlowRouter.route '/website/:id',
  name: 'website'
  action: ->
    BlazeLayout.render 'HomeLayout', main: 'website_single'
    return
